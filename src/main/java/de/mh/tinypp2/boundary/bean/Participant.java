package de.mh.tinypp2.boundary.bean;

import io.vertx.core.http.ServerWebSocket;

public class Participant {

    private final String id;
    private String name;
    private State state;
    private String lobbyId;
    private final ServerWebSocket webSocket;

    public Participant(String id, ServerWebSocket webSocket) {
        this.id = id;
        this.webSocket = webSocket;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(String lobbyId) {
        this.lobbyId = lobbyId;
    }

    //TODO: check how to properly send messages, maybe by dispatching to communicationHub via eventbus
    public void sendMessage(String message) {
        webSocket.writeTextMessage(message);
    }

    public static enum State {
        ARRIVED, JOINING, JOINED
    }
}
