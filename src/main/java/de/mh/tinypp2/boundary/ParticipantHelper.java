package de.mh.tinypp2.boundary;

import java.security.SecureRandom;

public final class ParticipantHelper {

    private ParticipantHelper() {
    }

    private static final String[] PREFIX1 = new String[]{"fluffy", "soft", "plushy", "tiny", "handsome", "shiny", "cute", "adorable", "nice", "friendly"};
    private static final String[] PREFIX2 = new String[]{"red", "blue", "green", "yellow", "pink", "magenta", "yellow", "cyan", "violet", "purple"};
    private static final String[] NOUN = new String[]{"guitar", "ball", "kettle", "toddler", "cat", "dog", "slide", "keyboard", "spaghetti", "noodle"};
    private static final String[] SUFFIX = new String[]{"from_space", "of_fluff", "with_hair", "with_cheese", "with_ears", "from_home", "of_the_world", "formerly_known_as_prince", "from_the_past", "of_happiness"};
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    public static String createParticipantId() {
        var id = new StringBuilder();

        // not very effective as id, but well
        id.append(randomElement(PREFIX1));
        id.append('_');
        id.append(randomElement(PREFIX2));
        id.append('_');
        id.append(randomElement(NOUN));
        id.append('_');
        id.append(randomElement(SUFFIX));

        return id.toString();
    }

    private static String randomElement(String[] array) {
        return array[(int) ((double) array.length * SECURE_RANDOM.nextDouble())];
    }
}
