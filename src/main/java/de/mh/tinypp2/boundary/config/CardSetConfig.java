package de.mh.tinypp2.boundary.config;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

//TODO: could be externalised into configuration
public final class CardSetConfig {

    private CardSetConfig() {
    }

    public static List<CardSet> getCardSets() {
        var cardSets = new LinkedList<CardSet>();
        cardSets.add(
                new CardSet(
                        "fib2", "Agile \"Fibonacci\"", true,
                        Arrays.asList(
                                new CardSet.Value("0", 0),
                                new CardSet.Value("1", 1),
                                new CardSet.Value("2", 2),
                                new CardSet.Value("3", 3),
                                new CardSet.Value("5", 5),
                                new CardSet.Value("8", 8),
                                new CardSet.Value("13", 13),
                                new CardSet.Value("20", 20),
                                new CardSet.Value("40", 40),
                                new CardSet.Value("100", 100)
                        ),
                        true
                ));
        cardSets.add(
                new CardSet(
                        "fib1", "Real Fibonacci", true,
                        Arrays.asList(
                                new CardSet.Value("0", 0),
                                new CardSet.Value("1", 1),
                                new CardSet.Value("2", 2),
                                new CardSet.Value("3", 3),
                                new CardSet.Value("5", 5),
                                new CardSet.Value("8", 8),
                                new CardSet.Value("13", 13),
                                new CardSet.Value("21", 21),
                                new CardSet.Value("34", 34),
                                new CardSet.Value("55", 55),
                                new CardSet.Value("89", 89)
                        ),
                        true
                ));
        cardSets.add(
                new CardSet(
                        "shirt", "T-shirt sizes", true,
                        Arrays.asList(
                                new CardSet.Value("XS", 1),
                                new CardSet.Value("S", 2),
                                new CardSet.Value("M", 3),
                                new CardSet.Value("L", 4),
                                new CardSet.Value("XL", 5),
                                new CardSet.Value("XXL", 6)
                        ),
                        false
                ));
        return cardSets;
    }

    public static CardSet getCardSetById(String id) {
        return getCardSets().stream()
                .filter(cardSet -> cardSet.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    public static class CardSet {

        private final String id;
        private final String name;
        private final boolean hasQuestionMark;
        private final List<Value> values;
        private final boolean hasNumericDisplayValues;

        public CardSet(String id, String name, boolean hasQuestionMark, List<Value> values, boolean hasNumericDisplayValues) {
            this.id = id;
            this.name = name;
            this.hasQuestionMark = hasQuestionMark;
            this.values = values;
            this.hasNumericDisplayValues = hasNumericDisplayValues;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public boolean isHasQuestionMark() {
            return hasQuestionMark;
        }

        public List<Value> getValues() {
            return values;
        }

        public boolean isHasNumericDisplayValues() {
            return hasNumericDisplayValues;
        }

        public static class Value {

            private final String displayValue;
            private final double numericValue;

            public Value(String displayValue, double numericValue) {
                this.displayValue = displayValue;
                this.numericValue = numericValue;
            }

            public String getDisplayValue() {
                return displayValue;
            }

            public double getNumericValue() {
                return numericValue;
            }
        }
    }
}
