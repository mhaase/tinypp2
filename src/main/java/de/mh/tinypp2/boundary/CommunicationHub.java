package de.mh.tinypp2.boundary;

import de.mh.tinypp2.boundary.bean.Participant;
import de.mh.tinypp2.boundary.config.CardSetConfig;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class CommunicationHub implements Handler<ServerWebSocket> {

    private final Vertx vertx;
    private final Map<String, Participant> participants;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public CommunicationHub(Vertx vertx) {
        this.vertx = vertx;
        this.participants = new HashMap<>();

        MessageConsumer<JsonObject> consumer = vertx.eventBus().consumer("boundary.communication-hub");
        consumer.handler(eventbusMessageConsumer());
    }

    @Override
    public void handle(ServerWebSocket socket) {
        var participantId = createUnusedParticipantId();

        if (participantId == null) {
            log.error("could not create participantId!");
            socket.writeTextMessage(
                    new JsonObject()
                            .put("type", "error")
                            .put("message", "could not register participant")
                            .encode()
            );
            return;
        }

        var participant = new Participant(participantId, socket);
        participant.setState(Participant.State.ARRIVED);
        participants.put(participantId, participant);

        socket.textMessageHandler(messageHandler(participant));
        socket.closeHandler(closeHandler(participant));
        //TODO: implement on close handling, along with cleanup of lobbies

        participant.sendMessage(
                new JsonObject()
                        .put("type", "welcome")
                        .put("pid", participantId)
                        .put("cardSets", CardSetConfig.getCardSets())
                        .encode()
        );
    }

    private String createUnusedParticipantId() {
        for (int i = 0; i < 10; i += 1) {
            var participantId = ParticipantHelper.createParticipantId();
            if (!participants.containsKey(participantId)) {
                return participantId;
            }
            log.warn("failed to create unused participantId in attempt [" + i + "], already used: [" + participantId + "]");
        }
        return null;
    }

    private Handler<String> messageHandler(Participant participant) {
        return message -> {

            log.info("received message [" + message + "]");

            var parsedMessage = new JsonObject(message);

            var type = parsedMessage.getString("type");
            if (type == null) {
                log.warn("invalid message, missing type attribute: [" + message + "]");
                return;
            }

            switch (type) {
                case "createAndJoinLobby" -> {
                    var participantName = parsedMessage.getString("pName");
                    var cardSetId = parsedMessage.getString("cardSetId");

                    if (participant.getState() != Participant.State.ARRIVED) {
                        log.warn("participant not in correct state [is:" + participant.getState() + "] to create lobby: [" + parsedMessage + "]");
                        return;
                    }
                    if (participantName == null || participantName.isEmpty()) {
                        log.warn("illegal participant name in message: [" + parsedMessage + "]");
                        return;
                    }
                    if (cardSetId == null || cardSetId.isEmpty() || CardSetConfig.getCardSetById(cardSetId) == null) {
                        log.warn("illegal cardSetId in message: [" + cardSetId + "]");
                        return;
                    }

                    participant.setState(Participant.State.JOINING);
                    participant.setName(participantName);

                    vertx.eventBus().send("control.lobby-manager",
                            new JsonObject()
                                    .put("type", "create-and-join-lobby")
                                    .put("pId", participant.getId())
                                    .put("cardSetId", cardSetId)
                    );
                }
                case "joinLobby" -> {
                    var participantName = parsedMessage.getString("pName");
                    var lobbyId = parsedMessage.getString("lobbyId");

                    if (participant.getState() != Participant.State.ARRIVED) {
                        log.warn("participant not in correct state [is:" + participant.getState() + "] to join lobby: [" + parsedMessage + "]");
                        return;
                    }
                    if (participantName == null || participantName.isEmpty()) {
                        log.warn("illegal participant name in message: [" + parsedMessage + "]");
                        return;
                    }
                    if (lobbyId == null || lobbyId.isEmpty()) {
                        log.warn("illegal lobbyId in message: [" + lobbyId + "]");
                        return;
                    }

                    participant.setState(Participant.State.JOINING);
                    participant.setName(participantName);

                    var fallbackCardSetId = CardSetConfig.getCardSets().get(0).getId();
                    vertx.eventBus().send("control.lobby-manager",
                            new JsonObject()
                                    .put("type", "join-lobby")
                                    .put("lobbyId", lobbyId)
                                    .put("pId", participant.getId())
                                    .put("fallbackCardSetId", fallbackCardSetId)
                    );
                }
                case "vote" -> {
                    var vote = parsedMessage.getString("vote");

                    if (participant.getState() != Participant.State.JOINED) {
                        log.warn("participant not in correct state [is:" + participant.getState() + "] to vote: [" + parsedMessage + "]");
                        return;
                    }
                    if (participant.getLobbyId() == null) {
                        log.warn("participant in correct state but without lobbyId during vote: [" + parsedMessage + "]");
                        return;
                    }

                    vertx.eventBus().send("control.lobby-manager",
                            new JsonObject()
                                    .put("type", "vote")
                                    .put("vote", vote)
                                    .put("lobbyId", participant.getLobbyId())
                                    .put("pId", participant.getId())
                    );
                }
                case "showVotes" -> {
                    if (participant.getState() != Participant.State.JOINED) {
                        log.warn("participant not in correct state [is:" + participant.getState() + "] to show votes: [" + parsedMessage + "]");
                        return;
                    }
                    if (participant.getLobbyId() == null) {
                        log.warn("participant in correct state but without lobbyId during showing votes: [" + parsedMessage + "]");
                        return;
                    }

                    vertx.eventBus().send("control.lobby-manager",
                            new JsonObject()
                                    .put("type", "show-votes")
                                    .put("lobbyId", participant.getLobbyId())
                                    .put("pId", participant.getId())
                    );
                }
                case "nextRound" -> {
                    if (participant.getState() != Participant.State.JOINED) {
                        log.warn("participant not in correct state [is:" + participant.getState() + "] to start next round: [" + parsedMessage + "]");
                        return;
                    }
                    if (participant.getLobbyId() == null) {
                        log.warn("participant in correct state but without lobbyId during starting next round: [" + parsedMessage + "]");
                        return;
                    }

                    vertx.eventBus().send("control.lobby-manager",
                            new JsonObject()
                                    .put("type", "next-round")
                                    .put("lobbyId", participant.getLobbyId())
                                    .put("pId", participant.getId())
                    );
                }
                default -> {
                    log.warn("unknown type in message: [" + message + "]");
                }
            }

        };
    }

    private Handler<Message<JsonObject>> eventbusMessageConsumer() {
        return eventbusMessage -> {
            var payload = eventbusMessage.body();

            var type = payload.getString("type");
            switch (type) {
                case "error" -> {
                    var to = payload.getString("to");
                    var participant = participants.get(to);
                    if (participant == null) {
                        log.warn("unknown recipient [" + to + "] for error message [" + payload.encode() + "]");
                        return;
                    }
                    var message = payload.getString("message");
                    participant.sendMessage(
                            new JsonObject()
                                    .put("type", "error")
                                    .put("message", message)
                                    .encode()
                    );
                }
                case "error-joining" -> {
                    var to = payload.getString("to");
                    var participant = participants.get(to);
                    if (participant == null) {
                        log.warn("unknown recipient [" + to + "] for error message [" + payload.encode() + "]");
                        return;
                    }

                    //reset state to enable next join attempt
                    participant.setState(Participant.State.ARRIVED);
                    var message = payload.getString("message");
                    participant.sendMessage(
                            new JsonObject()
                                    .put("type", "error")
                                    .put("message", message)
                                    .encode()
                    );
                }
                case "lobby-update" -> {
                    var lobbyId = payload.getString("lobbyId");
                    var cardSetId = payload.getString("cardSetId");
                    var participantList = payload.getJsonArray("participants");
                    var newParticipantIds = payload.getJsonArray("newParticipantIds");

                    var message = new JsonObject();
                    message.put("type", "lobbyUpdate");
                    message.put("lobbyId", lobbyId);
                    message.put("cardSet", CardSetConfig.getCardSetById(cardSetId));
                    message.put("newParticipantIds", newParticipantIds);
                    var participantDetails = new JsonArray();
                    message.put("participants", participantDetails);
                    var receivingParticipants = new LinkedList<Participant>();
                    participantList
                            .stream()
                            .map(obj -> (JsonObject) obj)
                            .forEach(p -> {
                                var participantId = p.getString("pId");
                                var vote = p.getString("vote");

                                var participant = participants.get(participantId);
                                if (participant == null) {
                                    return;
                                }
                                if (participant.getState() != Participant.State.JOINED) {
                                    participant.setState(Participant.State.JOINED);
                                    participant.setLobbyId(lobbyId);
                                }
                                receivingParticipants.add(participant);
                                participantDetails.add(
                                        new JsonObject()
                                                .put("pId", participantId)
                                                .put("pName", participant.getName())
                                                .put("vote", vote)
                                );
                            });

                    var messagePayload = message.encode();
                    receivingParticipants.forEach(participant -> {
                        participant.sendMessage(messagePayload);
                    });
                }
                default -> {
                    log.warn("received unknown message of type [" + payload.getClass().getSimpleName() + "]");
                }
            }
        };
    }

    private Handler<Void> closeHandler(Participant participant) {
        return _void -> {
            participants.remove(participant.getId());

            vertx.eventBus().send("control.lobby-manager",
                    new JsonObject()
                            .put("type", "leave")
                            .put("lobbyId", participant.getLobbyId())
                            .put("pId", participant.getId())
            );
        };
    }
}
