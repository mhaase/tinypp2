package de.mh.tinypp2;

import de.mh.tinypp2.control.LobbyManager;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;

public class LobbyVerticle extends AbstractVerticle {

    private LobbyManager lobbyManager;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        this.lobbyManager = new LobbyManager(vertx);
        
        log.info("started " + this.getClass().getSimpleName() + " with " + lobbyManager.getClass().getSimpleName());
        startPromise.complete();
    }
}
