package de.mh.tinypp2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import java.util.LinkedList;
import java.util.List;

public class VertxStarter {

    public static void main(String[] args) {
        var deploymentChain = new LinkedList<DeployableVerticle>();
        deploymentChain.add(new DeployableVerticle(LobbyVerticle.class, new DeploymentOptions().setInstances(1)));
        deploymentChain.add(new DeployableVerticle(CommunicationHubVerticle.class, new DeploymentOptions().setInstances(1)));

        new VertxStarter().deployVerticles(deploymentChain);
    }

    private final Vertx vertx;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public VertxStarter() {
        vertx = Vertx.vertx();
    }

    /**
     * Deploys the verticles in the given order, always waiting that the last
     * one succeeded. Fails on error of any deployment
     */
    void deployVerticles(List<DeployableVerticle> deployableVerticles) {
        var dv = deployableVerticles.get(0);
        log.info("deploying [" + dv.verticleClass().getSimpleName() + "]");
        vertx.deployVerticle(dv.verticleClass(), dv.deploymentOptions())
                .onSuccess(result -> {
                    log.info("started [" + dv.verticleClass().getSimpleName() + "] with id [" + result + "]");
                    if (deployableVerticles.size() > 1) {
                        deployVerticles(deployableVerticles.subList(1, deployableVerticles.size()));
                    }
                })
                .onFailure(ex -> {
                    log.error("failed to start [" + dv.verticleClass().getSimpleName() + "], aborting verticle deployment", ex);
                    vertx.close();
                });
    }

    private static record DeployableVerticle(Class<? extends AbstractVerticle> verticleClass, DeploymentOptions deploymentOptions) {

    }
}
