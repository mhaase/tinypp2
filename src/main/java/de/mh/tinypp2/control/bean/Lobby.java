package de.mh.tinypp2.control.bean;

import java.util.LinkedList;
import java.util.List;

public record Lobby(String id, String cardSetId, List<VotingParticipant> votingParticipants, Options options) {

    public Lobby(String id, String cardSetId) {
        this(id, cardSetId, new LinkedList<>(), new Options());
    }

    public static class VotingParticipant {

        private final String participantId;
        private String vote;

        public VotingParticipant(String participantId) {
            this.participantId = participantId;
        }

        public String getParticipantId() {
            return participantId;
        }

        public String getVote() {
            return vote;
        }

        public void setVote(String vote) {
            this.vote = vote;
        }
    }

    public static class Options {

        private boolean showVotes;

        public boolean isShowVotes() {
            return showVotes;
        }

        public void setShowVotes(boolean showVotes) {
            this.showVotes = showVotes;
        }
    }
}
