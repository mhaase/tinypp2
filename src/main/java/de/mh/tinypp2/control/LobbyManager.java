package de.mh.tinypp2.control;

import de.mh.tinypp2.boundary.config.CardSetConfig;
import de.mh.tinypp2.control.bean.Lobby;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class LobbyManager {

    private final Vertx vertx;
    private final MessageProducer<Object> responseSink;
    private final Map<String, Lobby> lobbies;
    private final Pattern validLobbyId = Pattern.compile("[A-Za-z0-9\\-_\\.]{1,32}");
    private static final int CREATE_LOBBY_RETRIES = 10;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public LobbyManager(Vertx vertx) {
        this.vertx = vertx;
        this.lobbies = new HashMap<>();

        MessageConsumer<JsonObject> consumer = this.vertx.eventBus().consumer("control.lobby-manager");
        consumer.handler(eventbusMessageConsumer());

        this.responseSink = vertx.eventBus().publisher("boundary.communication-hub");
    }

    private Lobby createLobby(String cardSetId, String desiredlobbyId) {
        if (!validLobbyId.matcher(desiredlobbyId).matches()) {
            log.warn("illegal lobbyId provided: [" + desiredlobbyId + "]");
            return null;
        }
        var lobby = new Lobby(desiredlobbyId, cardSetId);
        lobbies.put(desiredlobbyId, lobby);
        return lobby;
    }

    private Lobby createLobby(String cardSetId) {
        var lobbyId = createLobbyId();
        int retries = 0;
        while (findLobby(lobbyId) != null) {
            retries += 1;
            if (retries > CREATE_LOBBY_RETRIES) {
                return null;
            }
            lobbyId = createLobbyId();
        }
        var lobby = new Lobby(lobbyId, cardSetId);
        lobbies.put(lobbyId, lobby);
        return lobby;
    }

    private Handler<Message<JsonObject>> eventbusMessageConsumer() {
        return message -> {
            var payload = message.body();

            var type = payload.getString("type");
            switch (type) {
                case "create-and-join-lobby" -> {
                    var cardSetId = payload.getString("cardSetId");
                    var participantId = payload.getString("pId");
                    log.info("creating lobby with cardSetId [" + cardSetId + "] for participantId [" + participantId + "]");

                    var lobby = createLobby(cardSetId);
                    if (lobby == null) {
                        log.warn("could not create lobby with cardSetId [" + cardSetId + "] for participantId [" + participantId + "]");

                        responseSink.write(
                                new JsonObject()
                                        .put("type", "error-joining")
                                        .put("message", "could not create lobby")
                                        .put("pId", participantId)
                        );
                        return;
                    }
                    lobby.votingParticipants().add(new Lobby.VotingParticipant(participantId));

                    updateLobbyParticipants(lobby, Collections.singletonList(participantId));
                    logLobbyParticipantCount(lobby);
                }
                case "join-lobby" -> {
                    var lobbyId = payload.getString("lobbyId");
                    var participantId = payload.getString("pId");
                    var fallbackCardSetId = payload.getString("fallbackCardSetId");
                    log.info("join lobby with lobbyId [" + lobbyId + "] for participantId [" + participantId + "]");

                    var lobby = findLobby(lobbyId);
                    if (lobby == null) {
                        lobby = createLobby(fallbackCardSetId, lobbyId);
                        if (lobby == null) {
                            log.warn("could not join or create lobby with lobbyId [" + lobbyId + "] for participantId [" + participantId + "]");

                            responseSink.write(
                                    new JsonObject()
                                            .put("type", "error-joining")
                                            .put("message", "could not create lobby")
                                            .put("pId", participantId)
                            );
                            return;
                        }
                    }
                    lobby.votingParticipants().add(new Lobby.VotingParticipant(participantId));

                    updateLobbyParticipants(lobby, Collections.singletonList(participantId));
                    logLobbyParticipantCount(lobby);
                }
                case "vote" -> {
                    var vote = payload.getString("vote");
                    var participantId = payload.getString("pId");
                    var lobbyId = payload.getString("lobbyId");
                    log.info("vote [" + vote + "] with lobbyId [" + lobbyId + "] for participantId [" + participantId + "]");

                    var lobby = findLobby(lobbyId);
                    if (lobby == null) {
                        log.warn("could not vote: illegal lobby with lobbyId [" + lobbyId + "] for participantId [" + participantId + "]");

                        responseSink.write(
                                new JsonObject()
                                        .put("type", "error")
                                        .put("message", "could not vote: illegal lobby")
                                        .put("pId", participantId)
                        );
                        return;
                    }

                    if (!validVote(lobby, vote)) {
                        log.warn("not a valid vote [" + vote + "] in lobby with lobbyId [" + lobbyId + "] for participantId [" + participantId + "]");

                        responseSink.write(
                                new JsonObject()
                                        .put("type", "error")
                                        .put("message", "could not vote: illegal vote value")
                                        .put("pId", participantId)
                        );
                        return;
                    }

                    lobby.votingParticipants()
                            .stream()
                            .filter(vp -> vp.getParticipantId().equals(participantId))
                            .findAny()
                            .ifPresentOrElse(vp -> {
                                vp.setVote(vote);

                                var allParticipantsVoted = lobby.votingParticipants().stream()
                                        .allMatch(votingParticipant -> votingParticipant.getVote() != null);
                                if (allParticipantsVoted) {
                                    lobby.options().setShowVotes(true);
                                }
                                updateLobbyParticipants(lobby, Collections.emptyList());
                            }, () -> {
                                //no participant with matching id found. Strange
                                log.warn("could not vote: illegal participantId [" + participantId + "] for lobby with lobbyId [" + lobbyId + "]");
                            });
                }
                case "show-votes" -> {
                    var participantId = payload.getString("pId");
                    var lobbyId = payload.getString("lobbyId");
                    log.info("show votes for lobbyId [" + lobbyId + "] by participantId [" + participantId + "]");

                    var lobby = findLobby(lobbyId);
                    if (lobby == null) {
                        log.warn("could not show votes: illegal lobby with lobbyId [" + lobbyId + "] by participantId [" + participantId + "]");

                        responseSink.write(
                                new JsonObject()
                                        .put("type", "error")
                                        .put("message", "could not show votes: illegal lobby")
                                        .put("pId", participantId)
                        );
                        return;
                    }

                    lobby.options().setShowVotes(true);
                    updateLobbyParticipants(lobby, Collections.emptyList());
                }
                case "next-round" -> {
                    var participantId = payload.getString("pId");
                    var lobbyId = payload.getString("lobbyId");
                    log.info("next round for lobbyId [" + lobbyId + "] by participantId [" + participantId + "]");

                    var lobby = findLobby(lobbyId);
                    if (lobby == null) {
                        log.warn("could not start next round: illegal lobby with lobbyId [" + lobbyId + "] by participantId [" + participantId + "]");

                        responseSink.write(
                                new JsonObject()
                                        .put("type", "error")
                                        .put("message", "could not start next round: illegal lobby")
                                        .put("pId", participantId)
                        );
                        return;
                    }

                    lobby.options().setShowVotes(false);
                    lobby.votingParticipants().forEach(vp -> vp.setVote(null));
                    updateLobbyParticipants(lobby, Collections.emptyList());
                }
                case "leave" -> {
                    var participantId = payload.getString("pId");
                    var lobbyId = payload.getString("lobbyId");
                    log.info("someone is leaving! lobbyId [" + lobbyId + "] by participantId [" + participantId + "]");

                    var lobby = findLobby(lobbyId);
                    if (lobby == null) {
                        log.warn("trying to leave nonexistant lobby: lobbyId [" + lobbyId + "] by participantId [" + participantId + "]");

                        responseSink.write(
                                new JsonObject()
                                        .put("type", "error")
                                        .put("message", "could not leave lobby: illegal lobby")
                                        .put("pId", participantId)
                        );
                        return;
                    }

                    lobby.votingParticipants()
                            .stream()
                            .filter(vp -> vp.getParticipantId().equals(participantId))
                            .findAny()
                            .ifPresentOrElse(vp -> {
                                lobby.votingParticipants().remove(vp);
                                updateLobbyParticipants(lobby, Collections.emptyList());
                            }, () -> {
                                log.warn("trying to leave lobby, but participant is not in: lobbyId [" + lobbyId + "] by participantId [" + participantId + "]");

                                responseSink.write(
                                        new JsonObject()
                                                .put("type", "error")
                                                .put("message", "could not leave lobby: you are not in this lobby")
                                                .put("pId", participantId)
                                );
                            });
                    logLobbyParticipantCount(lobby);
                }
                default -> {
                    log.warn("received unknown message of type [" + payload.getClass().getSimpleName() + "]");
                }
            }
        };
    }

    private void logLobbyParticipantCount(Lobby lobby) {
        log.info("lobby [" + lobby.id() + "] now has [" + lobby.votingParticipants().size() + "] participants");
    }

    private boolean validVote(Lobby lobby, String vote) {
        return CardSetConfig.getCardSetById(lobby.cardSetId()).getValues()
                .stream()
                .anyMatch(v -> v.getDisplayValue().equals(vote));
    }

    private void updateLobbyParticipants(Lobby lobby, List<String> newParticipantIds) {
        var showVotes = lobby.options().isShowVotes();

        var participants = lobby.votingParticipants()
                .stream()
                .map(
                        vp -> new JsonObject()
                                .put("pId", vp.getParticipantId())
                                .put("vote", showVotes ? vp.getVote() : (vp.getVote() == null ? null : "")) //empty string means voted, but not visible
                )
                .toList();
        responseSink.write(
                new JsonObject()
                        .put("type", "lobby-update")
                        .put("lobbyId", lobby.id())
                        .put("cardSetId", lobby.cardSetId())
                        .put("participants", new JsonArray(participants))
                        .put("newParticipants", new JsonArray(newParticipantIds))
        );
    }

    private String createLobbyId() {
        return Integer.toString((int) (Math.random() * 100000));
    }

    private Lobby findLobby(String lobbyId) {
        return lobbies.get(lobbyId);
    }
}
