package de.mh.tinypp2;

import de.mh.tinypp2.boundary.ApiHandler;
import de.mh.tinypp2.boundary.CommunicationHub;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

public class CommunicationHubVerticle extends AbstractVerticle {

    private CommunicationHub communicationHub;
    private ApiHandler apiHandler;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        this.communicationHub = new CommunicationHub(vertx);
        this.apiHandler = new ApiHandler();

        Router router = Router.router(vertx);

        router.route(HttpMethod.GET, "/*").handler(StaticHandler.create());
        router.route(HttpMethod.GET, "/api/hello").handler(apiHandler.hello());

        vertx.createHttpServer().requestHandler(router).webSocketHandler(communicationHub::handle).listen(8888,
                http -> {
                    if (http.succeeded()) {
                        log.info("HTTP server listening on port [" + http.result().actualPort() + "]");
                        startPromise.complete();
                    } else {
                        log.error("HTTP server failed to start", http.cause());
                        startPromise.fail(http.cause());
                    }
                });
    }
}
