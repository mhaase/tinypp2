# tinypp2

A small agile poker tool, in its second iteration.

## tech stack

- Java 17
- vert.x 4.x
  - event-based programming using callbacks

## requirements

- [x] allow creation of lobbies
  - [x] provide lobby name (optional)
  - [x] provide user name
  - [ ] optional: provide topic for lobby
- [x] allow joining lobbies via code
  - [x] provide user name
- [x] allow joining lobbies via link
  - [x] provide user name
- [x] offer different presets of cards
- [x] login-less participation
- [x] choose from a set of cards and play one
  - [x] players are informed that someone played a card, but can't see it yet
  - [x] changing vote possible all the time - statistics need to be updated on the fly
- [x] cards are only shown when everyone has voted
  - [x] host (or everone) may get ability to show cards before (to combat AFK participants)
- [x] shown cards can be reset for next round
- [x] statistics about the voting result

## requirements (non-functional)

- [x] basic UI
- [x] possibility to use UI mouseless
- [x] overview about possible events
- [x] scalability (use vert.x tooling, maybe persistence)
- [ ] be GDPR friendly :)

## current todos / next steps / wip

- [ ] handle closed connections
- [ ] add anchor when creating/joining a lobby
- [ ] make the statistics screen a bit nicer looking
- [ ] make the shortcuts of cards a bit nicer looking
  - [ ] shortcuts for voting could be selected with arrows, WASD oder hjkl
- [ ] handle error messages from the server
- [ ] implement '?' vote (optional: coffee cup vote)
- [ ] currently ctrl+c acts as 'c', there might be unintended actions performed on the page by performing other (browser/OS) shortcuts
- [ ] consider splitting the lobbyState event (e.g. lobbyWelcome, lobbyVote, lobbyJoined)

## requirements for productive go-live
- [ ] environments / properties
- [ ] SSL handling (http + websocket); HSTS
- [ ] be legally compliant (imprint, data privacy etc.)
- [ ] texts, help, documentation
- [ ] pictures
- [ ] i18n (?)

## used events

### client -> server

- createAndJoinLobby {pName, cardSetId}
- joinLobby {pName, lobbyId}
- vote {value}
- showVotes
- nextRound

### server -> client

- welcome {pid, cardSets}
- lobbyState {lobbyId, participants: {pid, pName, vote}, newParticipantIds}
